package controller

import (
	"crypto/md5"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"regexp"

	"login6.0/dao"
	"login6.0/model"
	"login6.0/utils"
)

func Login(w http.ResponseWriter, r *http.Request) {
	var user model.User
	// 解析请求
	switch r.Method {
	case http.MethodPost:
		dec := json.NewDecoder(r.Body)
		err := dec.Decode(&user)
		if err != nil {
			log.Println(err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}
	// 判断格式
	reg1, _ := regexp.MatchString(`^1\d{10}`, user.Mobile)       // 手机号
	reg3, _ := regexp.MatchString(`^[\w_]{6,20}`, user.Password) // 密码必须是6-20位的字母、数字或下划线
	if !(reg1 && reg3) {
		//格式有误
		rst := model.Result{
			Code: 500,
			Msg:  "登陆失败，手机号或密码不合法",
			Data: []string{},
		}
		response, _ := json.Marshal(rst)  // json化结果集
		fmt.Fprintln(w, string(response)) // 返回结果
		return
	} else {
		//格式正确，查询手机号和密码是否匹配
		user.Password = fmt.Sprintf("%x", md5.Sum([]byte(user.Password)))
		user, err := dao.CheckUserMobileAndPassword(user.Mobile, user.Password)
		if err != nil {
			//手机号或密码不正确
			rst := model.Result{
				Code: 500,
				Msg:  "登陆失败，手机号或密码不正确",
				Data: []string{},
			}
			response, _ := json.Marshal(rst)  // json化结果集
			fmt.Fprintln(w, string(response)) // 返回结果
			return
		} else {
			//用户名和密码正确
			token, err := utils.GenerateToken(user.Mobile, user.Name)
			if err != nil {
				rst := model.Result{
					Code: 500,
					Msg:  "未成功生成Token",
					Data: []string{},
				}
				response, _ := json.Marshal(rst)  // json化结果集
				fmt.Fprintln(w, string(response)) // 返回结果
			}

			// Token储存方式1:将Token存进cookie里
			cookie := http.Cookie{
				Name:  "dorm_user",
				Value: token,
				//HttpOnly: true,
			}
			//将cookie发送给浏览器
			http.SetCookie(w, &cookie)

			// Token储存方式2:将Token存进localstorage里

			//返回成功消息
			rst := model.Result{
				Code: 200,
				Msg:  "登陆成功",
				Data: []string{token},
			}
			response, _ := json.Marshal(rst)  // json化结果集
			fmt.Fprintln(w, string(response)) // 返回结果
			return
		}
	}
}
