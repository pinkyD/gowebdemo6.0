package main

import (
	"net/http"

	"login6.0/controller"
)

func main() {
	// http.HandleFunc("/register", controller.Register)
	http.HandleFunc("/login", controller.Login)
	http.HandleFunc("/list", controller.ListDorms)
	http.ListenAndServe(":8082", nil)
}
