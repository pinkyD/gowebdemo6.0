package dao

import (
	"login6.0/model"
	"login6.0/utils"
)

func CheckUserMobileAndPassword(mobile string, password string) (*model.User, error) {

	sqlStr := "select id, mobile, name, password from user where mobile = ? and password = ?"
	row := utils.UserDb.QueryRow(sqlStr, mobile, password)
	user := &model.User{}
	err := row.Scan(&user.ID, &user.Mobile, &user.Name, &user.Password)
	if err != nil {
		return user, err
	}
	return user, nil
}
