package dao

import (
	"login6.0/model"
	"login6.0/utils"
)

func GetDorms() ([]*model.Dorm, error) {

	sql := "select dorm_number, sex, total, available from dorm"

	rows, err := utils.DormDb.Query(sql)
	if err != nil {
		return nil, err
	}
	var dorms []*model.Dorm
	for rows.Next() {
		dorm := &model.Dorm{}
		rows.Scan(&dorm.DormNumber, &dorm.Sex, &dorm.Total, &dorm.Available)
		dorms = append(dorms, dorm)
	}
	return dorms, nil
}
