package model

type Dorm struct {
	DormNumber int
	Sex        string
	Total      int
	Available  int
}
