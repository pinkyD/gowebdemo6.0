package dao

import (
	"gowebdemo5.0/model"
	"gowebdemo5.0/utils"
)

func CheckUserMobileAndPassword(mobile string, password string) (*model.User, error) {

	sqlStr := "select id, mobile, name, password from user where mobile = ? and password = ?"
	row := utils.Db.QueryRow(sqlStr, mobile, password)
	user := &model.User{}
	err := row.Scan(&user.ID, &user.Mobile, &user.Name, &user.Password)
	if err != nil {
		return user, err
	}
	return user, nil
}

// func AddUser(user *model.User) error {
// 	sqlStr := "insert into users(mobile, name, password) values(?,?,?)"
// 	_, err := utils.Db.Exec(sqlStr, user.Mobile, user.Name, user.Password)
// 	if err != nil {
// 		return err
// 	}
// 	return nil
// }
