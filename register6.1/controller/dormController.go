package controller

import (
	"fmt"
	"net/http"

	"register6.0/dao"
	"register6.0/utils"
)

func ListDorms(w http.ResponseWriter, r *http.Request) {

	// 替换成解析验证token
	flag, _ := utils.IsLogin(r)

	if flag {
		fmt.Fprintf(w, "DormNO.\tSex\tTotal\tAvailable\n")
		dorms, _ := dao.GetDorms()
		for _, dorm := range dorms {
			fmt.Fprintf(w, "%v\t%v\t%v\t%v\n", dorm.DormNumber, dorm.Sex, dorm.Total, dorm.Available)
		}
	} else {
		fmt.Fprintf(w, "请先登录")
	}
}
