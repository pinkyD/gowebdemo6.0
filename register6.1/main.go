package main

import (
	"net/http"

	"register6.0/controller"
)

func main() {
	http.HandleFunc("/register", controller.Register)
	// http.HandleFunc("/login", controller.Login)
	// http.HandleFunc("/list", controller.ListDorms)
	http.ListenAndServe(":8081", nil)
}
