-- 建库
CREATE DATABASE IF NOT EXISTS db default charset utf8 COLLATE utf8_general_ci;

-- 切换数据库
use db;

-- 建user表
DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `mobile` varchar(11) NOT NULL COMMENT '手机号',
  `name` varchar(64) NOT NULL COMMENT '用户名',
  `password` varchar(64) NOT NULL COMMENT 'MD5密码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户信息表';

-- 建dorm表
DROP TABLE IF EXISTS `dorm`;

CREATE TABLE `dorm` (
  `dorm_number` int(10) NOT NULL AUTO_INCREMENT COMMENT '宿舍号',
  `sex` varchar(1) NOT NULL COMMENT '性别:F/M',
  `total` int(2) NOT NULL COMMENT '总容量',
  `available` int(2) NOT NULL COMMENT '剩余容量',
  PRIMARY KEY (`dorm_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='宿舍列表';

-- 插入数据
INSERT INTO 
dorm (dorm_number, sex, total, available) 
VALUES 
('5101', 'M', 4, 4),
('5102', 'M', 4, 4),
('5103', 'M', 4, 4),
('5104', 'M', 4, 4),
('5105', 'M', 4, 4),
('5201', 'F', 4, 4),
('5202', 'F', 4, 4),
('5203', 'F', 4, 4),
('5204', 'F', 4, 4),
('5205', 'F', 4, 4);